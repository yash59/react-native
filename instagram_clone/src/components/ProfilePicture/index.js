import React from "react";
import { View, Image } from "react-native";

import styles from "./styles";

const ProfilePicture = ({uri, size=60}) => {
    return (
        <View style={[styles.ppView, {width:size+8, height:size+8}]}>
            <Image
            style={[styles.image, {width:size, height:size}]}
            source={{uri}} />
        </View>
    );
}

export default ProfilePicture;
import React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";

const ListItem = (props) => {
    return (
        <TouchableOpacity>
            <View style={styles.listItemView}>
                <Text style={styles.listItemtext}>{props.prod}</Text>
                <Icon name="remove" size={20} color="firebrick" onPress={() => props.deleteItem(props.id)}/>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    listItemView: {
        padding: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#f8f8f8",
        borderBottomWidth: 2,
        borderColor: '#eee',
    },
    listItemtext: {
        color: "black",
        fontSize: 20,
    }
})

export default ListItem;
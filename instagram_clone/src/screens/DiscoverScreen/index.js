import React from 'react'
import { View, Text, StyleSheet } from 'react-native';

const DiscoverScreen = () => {
    return (
        <View style={styles.discoverView}>
            <Text style={styles.text}>This is Discovery Screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    discoverView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    text: {
        color: "white",
        fontSize: 25,
    }
})

export default DiscoverScreen;
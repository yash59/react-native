import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    ppView: {
        margin: 10,
        borderRadius: 76/2,
        // story style
        borderColor: "#8a36ad",
        borderWidth: 4,
    },
    image:{
        borderRadius: 70/2,
        borderColor: "#ffffff",
        borderWidth: 3,
    }
});

export default styles;
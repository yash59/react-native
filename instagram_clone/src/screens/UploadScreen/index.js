import React, { useState } from 'react'
import { View, StyleSheet, TextInput } from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from "react-native-vector-icons/Ionicons";

const UploadScreen = () => {

    const [input, setInput] = useState('');

    const handleChange = (text) => {
        setInput(text);
    }

    return (
        <View style={styles.uploadView}>
            <TextInput
            style={styles.inputField}
            placeholder="What's on your mind ?"
            multiline
            numberOfLines={4}
            onChangeText={handleChange}
            />
            <ActionButton buttonColor="rgba(231,76,60,1)">
                <ActionButton.Item
                buttonColor='#9b59b6'
                title="Take Photo"
                onPress={() => console.log("notes tapped!")}>
                <Icon name="camera-outline" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item
                buttonColor='#3498db'
                title="Choose Photo"
                onPress={() => {}}>
                <Icon name="md-images-outline" style={styles.actionButtonIcon} />
                </ActionButton.Item>
            </ActionButton>
        </View>
    );
}

const styles = StyleSheet.create({
    uploadView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    inputField: {
        textAlign: 'center',
        fontSize: 24,
        width: '90%',
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
})

export default UploadScreen;
import React from "react";
import { View } from "react-native";

import PostHeader from "./PostHeader";
import PostFooter from "./PostFooter"
import PostBody from "./PostBody";

const Post = (props) => {
    return (
            <View>
                <PostHeader
                // key={post.id}
                userImgUri={props.userImgUri}
                userName={props.userName}
                />
                <PostBody
                // key={post.id}
                imageUri={props.image}
                />
                <PostFooter
                // key={post.id}
                userName={props.userName}
                likes={props.likes}
                caption={props.caption}
                postedAt={props.postedAt}
                />
            </View>
    )
}

export default Post;
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from "react-native-vector-icons/Ionicons";

const PostFooter = (props) => {

    const [isLiked, setIsLiked] = useState(false);
    const [isSaved, setIsSaved] = useState(false);
    const [likesCount, setLikes] = useState(0);

    useEffect(() => {
        setLikes(props.likes);
    }, [])

    const onLikedPress = () => {
        setIsLiked(!isLiked);
        if(isLiked) setLikes(likesCount => likesCount-1);
        else setLikes(likesCount => likesCount+1);
    }

    const onSavePress = () => {
        setIsSaved(!isSaved);
    }

    return (
        <View style={styles.footerView}>
            <View style={styles.iconsView}>
                <View style={styles.leftIcons}>
                    <TouchableOpacity onPress={() => onLikedPress()}>
                        {
                            !isLiked ? <Icon name="heart-o" size={25} color={"#545454"} /> : (
                                <Icon name="heart" size={25} color={"#c30000"} />
                            )
                        }
                        
                    </TouchableOpacity>
                    <Icon name="comment-o" size={25} />
                    <IonIcon name="paper-plane-outline" size={25} />
                </View>
                <View style={styles.rightIcons}>
                    <TouchableOpacity onPress={() => onSavePress()}>
                        {
                            !isSaved ? <Icon name="bookmark-o" size={25} color={"#545454"} /> : (
                                <Icon name="bookmark" size={25} color={"#00bf38"} />
                            )
                        }
                    </TouchableOpacity>
                </View>
            </View>
            <Text style={styles.likes}>{likesCount} likes</Text>
            <Text style={styles.name}>{props.userName} <Text style={styles.caption}>{props.caption}</Text></Text>
            <Text style={styles.postedAt}>{props.postedAt}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    footerView: {
        margin: 5,
    },
    iconsView: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    leftIcons: {
        flexDirection: 'row',
        width: 120,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    rightIcons: {
        marginRight: 15,
    },
    likes: {
        fontWeight: 'bold',
        margin: 3,
    },
    name: {
        fontWeight: 'bold',
        color: 'black',
        margin: 3,
    },
    caption: {
        fontWeight: '600',
        margin: 3,
    },
    postedAt: {
        color: 'gray',
        margin: 3,
    }
})

export default PostFooter;
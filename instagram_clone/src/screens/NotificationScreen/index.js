import React from 'react'
import { Text, View, StyleSheet } from 'react-native';

const NotificationScreen = () => {
    return (
        <View style={styles.notificationView}>
            <Text style={styles.text}>This is Notification Screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    notificationView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
    },
    text: {
        color: "white",
        fontSize: 25,
    }
})

export default NotificationScreen;
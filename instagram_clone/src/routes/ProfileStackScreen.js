import React from "react";
// react navigation
import { createStackNavigator } from '@react-navigation/stack';
// screens
import ProfileScreen from '../screens/ProfileScreen';

const ProfileStack = createStackNavigator();

const ProfileStackScreen = () => {
    return (
        <ProfileStack.Navigator>
            <ProfileStack.Screen 
            name="Profile"
            component={ProfileScreen}
            options={{
                headerTitleAlign: 'center'
            }}
            />
        </ProfileStack.Navigator>
    );
}

export default ProfileStackScreen;
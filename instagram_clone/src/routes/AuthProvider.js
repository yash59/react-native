import React, { createContext, useState } from 'react';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-community/google-signin';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    return (
        <AuthContext.Provider
        value={{
            user,
            setUser,
            signIn: async () => {
                try {
                  const { idToken } = await GoogleSignin.signIn();
                  const googleCredential = auth.GoogleAuthProvider.credential(idToken);
                  await auth().signInWithCredential(googleCredential);
                } catch (error) {
                  console.log(error.message);
                }
            },
            signout: async () => {
                try {
                    await auth().signOut();
                } catch (error) {
                    console.log(error.message);
                }
            }
            }
        }
        >
            {children}
        </AuthContext.Provider>
    )
}
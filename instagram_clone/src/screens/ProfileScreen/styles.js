import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    profileView: {
        flex: 1,
    },
    profileSection: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    profileSection__settings: {
        marginRight: 15,
    },
    profileSection__settings__followings: {
        width: 250,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 15,
    },
    profileNumbers: {
        fontSize: 21,
        fontWeight: 'bold',
    },
    profileBio: {
        padding: 10,
        marginTop: 5,
        marginLeft: 5,
    },
    profileBio__username: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    profileBio__bio: {
        fontWeight: '500',
    },
    profilePosts: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default styles;
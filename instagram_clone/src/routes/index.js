import 'react-native-gesture-handler';
import React, { useContext, useState, useEffect } from "react";
// react-native navigation
import { NavigationContainer } from '@react-navigation/native';
// authentication
import auth from '@react-native-firebase/auth';
// screens
import Router from "./Router";
import AuthStackScreen from './AuthStackScreen';
// authenticattion provider
import { AuthContext } from './AuthProvider';

const Routes = () => {

  const {user, setUser} = useContext(AuthContext);
  const [initializing, setInitializing] = useState(true);

  const onAuthStateChanged = (user) => {
    setUser(user);
    if(initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if(initializing) return null //when our app is establishing connection with firebase, return null or can show loading animation here...

  return (
    <NavigationContainer>
        {user ? <Router />: <AuthStackScreen />}
    </NavigationContainer>
  )
}

export default Routes;
import React, { useState } from "react";
import { View, StyleSheet, FlatList, Alert } from "react-native";
import { v4 as uuidv4 } from "uuid";

import Header from "./src/components/Header";
import ListItem from "./src/components/ListItem";
import AddItem from "./src/components/AddItem";

const App = () => {

  const [items, setItems] = useState([]);

  const deleteItem = (id) => {
    setItems(prevItems => {
      return prevItems.filter(item => item.id !== id);
    });
  }

  const addItem = (text) => {
    if(!text) {
      Alert.alert("Error", "Please Enter an Item", {
      text: "Okay"
      })
    }
    else {
      setItems(prevItems => {
        return [{id: uuidv4(), product:text}, ...prevItems];
      })
    }
  }

  return (
    <View style={styles.container}>
      <Header />
      <AddItem addItem={addItem} />
      <FlatList 
        data={items}
        renderItem={({item}) => <ListItem prod={item.product} deleteItem={deleteItem} id={item.id}/>}
      />
    </View>
  )
}

// Header Props
Header.defaultProps = {
  title: "To Do List",
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default App;
import React, { useEffect, useContext } from 'react'
import { View, Image } from 'react-native'
// logo
import logo from "../../assets/logo.png";
// styles
import styles from "./styles";
// authentication
import {
    GoogleSignin,
    GoogleSigninButton
} from '@react-native-community/google-signin';
// context provider
import { AuthContext } from "../../routes/AuthProvider";

const SignInScreen = () => {

    const { signIn } = useContext(AuthContext);

    useEffect(() => {
        GoogleSignin.configure({
            webClientId: '57744126722-dioc9a0mjvkpgbs7peno4gohoi1iht3l.apps.googleusercontent.com',
          });          
    }, [])

    return (
        <View style={styles.signInView}>
            <Image style={styles.logo} source={logo}/>
            <GoogleSigninButton
            style={{ width: 200, height: 60 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={signIn} 
            />
        </View>
    )
}

export default SignInScreen;
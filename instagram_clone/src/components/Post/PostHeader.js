import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Entypo";

import ProfilePicture from "../ProfilePicture";

const PostHeader = ({userImgUri, userName}) => {
    return (
        <View style={styles.headerView}>
            <View style={styles.leftHeaderView}>
                <ProfilePicture uri={userImgUri} size={40} />
                <Text style={styles.textName}>{userName}</Text>
            </View>
            <View style={styles.rightHeaderView}>
                <Icon name="dots-three-vertical" size={20} color="black" />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    headerView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
    },
    leftHeaderView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rightHeaderView: {
        marginRight: 15,
    },
    textName: {
        fontWeight: 'bold',
        color: "#4f4f4f",
    }
});

export default PostHeader;
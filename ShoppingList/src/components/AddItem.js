import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";

const AddItem = (props) => {

    const [input, setInput] = useState("");

    const handleChange = (value) => {
        setInput(value);
    }

    return (
        <View style={styles.addItemView}>
            <TextInput style={styles.addItemInput} placeholder="Add an Item..." onChangeText={handleChange}/>
            <TouchableOpacity onPress={() => props.addItem(input)}>
                <View style={styles.addItemBtn}>
                    <Text style={styles.addItemBtnTxt}><Icon name="plus" size={20} color="green"/> Add Item</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    addItemView: {
        padding: 15,
        marginBottom: 5,
    },
    addItemInput: {
        fontSize: 17,
    },
    addItemBtn: {
        backgroundColor: "lightgreen",
        padding: 15,
    },
    addItemBtnTxt: {
        textAlign: "center",
    }
});

export default AddItem;
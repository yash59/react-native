import 'react-native-gesture-handler';
import React from "react";
import { StatusBar } from "react-native";
// for stack and tab navigation
import Routes from './src/routes';
// authenticattion provider
import { AuthProvider } from './src/routes/AuthProvider';

const App = () => {

  return (
    <AuthProvider>
      <StatusBar barStyle='light-content' />
      <Routes />
    </AuthProvider>
  )
}

export default App;
import React from 'react'
// react navigation
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// icons
import FoundationIcon from 'react-native-vector-icons/Foundation';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from "react-native-vector-icons/FontAwesome";
// screens
import HomeStackScreen from './HomeStackScreen';
import ProfileStackScreen from './ProfileStackScreen';
import UploadScreen from "../screens/UploadScreen";
import DiscoverScreen from "../screens/DiscoverScreen";
import NotificationScreen from "../screens/NotificationScreen";

const Tab = createBottomTabNavigator();

const Router = () => {
    return (
        <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                if (route.name === 'Home') {
                return <FoundationIcon name="home" size={size} color={color}/>
                }
                if (route.name === 'Discover') {
                return <FeatherIcon name="search" size={size} color={color}/>
                }
                if (route.name === 'Upload') {
                return <FeatherIcon name="plus-square" size={size} color={color}/>
                }
                if (route.name === 'Notification') {
                return <Icon name="heart-o" size={size} color={color}/>
                }
                if (route.name === 'Profile') {
                return <Ionicons name="person-outline" size={size} color={color}/>
                }
            },
            })}
            tabBarOptions={{
                activeTintColor: 'black',
                inactiveTintColor: 'gray',
                showLabel: false,
            }}
            >
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Discover" component={DiscoverScreen} />
            <Tab.Screen name="Upload" component={UploadScreen} />
            <Tab.Screen name="Notification" component={NotificationScreen} />
            <Tab.Screen name="Profile" component={ProfileStackScreen} />
        </Tab.Navigator>
    )
}

export default Router;
import React from "react";
import { FlatList } from "react-native";

import Stories from "../../components/Stories";
import Post from "../Post";

const posts = [
    {   
        id: 0,
        user_name: 'Yash',
        user_dp: 'https://i.pravatar.cc/300',
        image: 'https://i.pravatar.cc/500',
        caption: 'Building Things...',
        likesCount: 500,
        postedAt: '1 min ago'
    },
    {   
        id: 1,
        user_name: 'Utkarsh',
        user_dp: 'https://i.pravatar.cc/300',
        image: 'https://i.pravatar.cc/500',
        caption: 'Thinking Broad...',
        likesCount: 400,
        postedAt: '2 min ago'
    },
    {   
        id: 2,
        user_name: 'Bobby',
        user_dp: 'https://i.pravatar.cc/300',
        image: 'https://i.pravatar.cc/500',
        caption: 'Brainstorming Ideas...',
        likesCount: 300,
        postedAt: '3 min ago'
    }
];

const Feed = () => {
    return (
        <FlatList
            ListHeaderComponent={Stories}
            data={posts}
            keyExtractor={({id}) => id.toString()}
            renderItem={({item}) => (
                <Post
                userName={item.user_name}
                userImgUri={item.user_dp}
                image={item.image}
                likes={item.likesCount}
                caption={item.caption}
                postedAt={item.postedAt}
                />
            )}
            />
    )
}

export default Feed;
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    signInView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        height: 80,
        width: 250,
        marginBottom: 100,
    },
});

export default styles;
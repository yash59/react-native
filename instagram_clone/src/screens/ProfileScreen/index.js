import React, { useContext } from 'react'
import { Text, View, Button } from 'react-native';
// styles
import styles from "./styles";
import ProfilePicture from "../../components/ProfilePicture";
// athenticated user
import { AuthContext } from "../../routes/AuthProvider";

const ProfileScreen = () => {

    const { user, signout } = useContext(AuthContext);

    return (
        <View style={styles.profileView}>
            <View style={styles.profileSection}>
                <ProfilePicture uri={user.photoURL}/>
                <View style={styles.profileSection__settings}>
                    <View style={styles.profileSection__settings__followings}>
                        <View>
                            <Text style={styles.profileNumbers}>224</Text>
                            <Text>Posts</Text>
                        </View>
                        <View>
                            <Text style={styles.profileNumbers}>9999</Text>
                            <Text>Followers</Text>
                        </View>
                        <View>
                            <Text style={styles.profileNumbers}>150</Text>
                            <Text>Following</Text>
                        </View>
                    </View>
                    <Button title='Edit Profile' />
                </View>
            </View>
            <View style={styles.profileBio}>
                <Text style={styles.profileBio__username}>{user.displayName}</Text>
                <Text style={styles.profileBio__bio}>Born on 7 April, 1999</Text>
                <Text style={styles.profileBio__bio}>Know what to do...</Text>
            </View>
            <View style={styles.profilePosts}>
                <Button
                title='Sign Out'
                onPress={signout}
                />
            </View>
        </View>
    );
}

export default ProfileScreen;
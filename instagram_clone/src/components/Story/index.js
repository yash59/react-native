import React from "react";
import { View, Text } from "react-native";

import ProfilePicture from "../ProfilePicture";
import styles from "./styles";

const Story = ({imgUri, name}) => {
    return (
        <View style={styles.StoryView}>
            <ProfilePicture uri={imgUri} />
            <Text>{name}</Text>
        </View>
    )
}

export default Story;
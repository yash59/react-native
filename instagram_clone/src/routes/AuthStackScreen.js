import React from "react";
import { Text } from "react-native";
// react navigation
import { createStackNavigator } from '@react-navigation/stack';
// signin screen
import SignInScreen from "../screens/SignInScreen";

const AuthStack = createStackNavigator();

const AuthStackScreen = () => {
    return (
        <AuthStack.Navigator>
            <AuthStack.Screen 
            name="Sign In/Log In"
            component={SignInScreen}
            options={{
                headerTitle: <Text>Sign In/Log In</Text>,
                headerTitleAlign: 'center'
            }}
            />
        </AuthStack.Navigator>
    )
}

export default AuthStackScreen;
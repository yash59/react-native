import React from "react";
import { View, FlatList, StyleSheet } from "react-native";

import Story from "../Story";

const stories = [
    {
        id: 0,
        image_url: 'https://i.pravatar.cc/300',
        username: 'React'
    },
    {
        id: 1,
        image_url: 'https://i.pravatar.cc/300',
        username: 'React Native'
    },
    {
        id: 2,
        image_url: 'https://i.pravatar.cc/300',
        username: 'Python'
    },
    {
        id: 3,
        image_url: 'https://i.pravatar.cc/300',
        username: 'Node Express'
    },
    {
        id: 4,
        image_url: 'https://i.pravatar.cc/300',
        username: 'Django'
    },
    {
        id: 5,
        image_url: 'https://i.pravatar.cc/300',
        username: 'Material Ui'
    }
];

const Stories = () => {
    return (
        <View style={styles.storiesView}>
            <FlatList
            data={stories}
            horizontal
            showsHorizontalScrollIndicator={false}
            renderItem={
                ({item}) => <Story
                imgUri={item.image_url}
                name={item.username}
                />
            }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    storiesView: {
        marginBottom: 8,
    }
})

export default Stories;
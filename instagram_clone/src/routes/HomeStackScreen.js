import React from "react";
import { Text } from "react-native";
// react navigation
import { createStackNavigator } from '@react-navigation/stack';
// icons
import FeatherIcon from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
// screens
import HomeScreen from "../screens/HomeScreen";

const HomeStack = createStackNavigator();

const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen
            name="Home"
            component={HomeScreen}
            options={{
                headerTitle: <Text style={{fontFamily: 'instafont'}}>Instagram</Text>,
                headerTitleAlign: 'center',
                headerLeftContainerStyle: {marginLeft: 15},
                headerRightContainerStyle: {marginRight: 15},
                headerLeft: () => <Ionicons name="md-camera-outline" size={25} color="black"/>,
                headerRight: () => <FeatherIcon name="search" size={25} color="black"/>,
            }}
            />
        </HomeStack.Navigator>
    )
}

export default HomeStackScreen;